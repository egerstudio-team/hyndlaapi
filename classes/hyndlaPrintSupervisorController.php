<?php namespace EgerStudio\HyndlaAPI\Classes;

use Illuminate\Routing\Controller;
use EgerStudio\HyndlaAPI\Models\ApiClient;
use EgerStudio\HyndlaAPI\Models\PrintJob;
use EgerStudio\HyndlaAPI\Models\PrintInstance;
use EgerStudio\HyndlaAPI\Models\Janitor;
use Response;
use Request;
use Input;
use Log as SystemLog;
use GuzzleHttp\Client;

// This would reside in your plugin's "classes" directory
class HyndlaPrintSupervisorController extends Controller
{
    //*
    /* This class is responsible for cleaning up print jobs that have for one
    /* reason or another been discarded or overlooked.
    /* The service should run every minute and do a simple DB call and reprint
    /* the dead print jobs.
    */


    public function simpleCleanAndPrintExt() {
      $janitor = new Janitor;
      $janitor->simpleCleanAndPrint();
    }


    




}
