<?php namespace EgerStudio\HyndlaAPI\Classes;

use Illuminate\Routing\Controller;
use EgerStudio\HyndlaAPI\Models\ApiClient;
use EgerStudio\HyndlaAPI\Models\PrintJob;
use EgerStudio\HyndlaAPI\Models\PrintInstance;
use Response;
use Request;
use Input;
use Log as SystemLog;

// This would reside in your plugin's "classes" directory
class HyndlaPrintOrderController extends Controller
{


    public function newOrder(){

      $vpnIp = Input::get('vpnIp');
      $apiKey = Input::get('apiKey');
      $meta = Input::get('meta');
      $duplicate = Input::get('duplicate');
      SystemLog::info('newOrder started, has apiKey('.$apiKey.') and vpnIp('.$vpnIp.'), we log content just after this.');
      SystemLog::info(urldecode(Input::get('content')));

      $job = new PrintJob;
      $client = ApiClient::where('api_key','=',$apiKey)->firstOrFail();

        $job->template = 'orderNew';
        $job->content = urldecode(Input::get('content'));
        $job->apiclient = $client;
        $job->save();
        SystemLog::info('We have a new job and saved it');

      $print = new PrintInstance;
      SystemLog::info('New PrintInstance');
      $print->addPrint($job);
      SystemLog::info('Added printjob '.$job);
      $print->sendToPrint($job,$client,$meta,$print->id,$duplicate);
      SystemLog::info('Sent to print '.$job->id);

    }


    public function updateOrder(){

      $vpnIp = Input::get('vpnIp');
      $apiKey = Input::get('apiKey');
      $meta = Input::get('meta');
      $duplicate = 0;
      SystemLog::info('updateOrder started, has apiKey('.$apiKey.') and vpnIp('.$vpnIp.'), we log content just after this.');
      SystemLog::info(urldecode(Input::get('content')));

      $job = new PrintJob;
      $client = ApiClient::where('api_key','=',$apiKey)->firstOrFail();

        $job->template = 'orderUpdate';
        $job->content = urldecode(Input::get('content'));
        $job->apiclient = $client;
        $job->save();

      $print = new PrintInstance;
      $print->addPrint($job);
      $print->sendToPrint($job,$client,$meta,$print->id,$duplicate);

    }


    public function deleteOrder(){

      $vpnIp = Input::get('vpnIp');
      $apiKey = Input::get('apiKey');
      $meta = Input::get('meta');
      $duplicate = 0;
      SystemLog::info('deleteOrder started, has apiKey('.$apiKey.') and vpnIp('.$vpnIp.'), we log content just after this.');
      SystemLog::info(urldecode(Input::get('content')));

      $job = new PrintJob;
      $client = ApiClient::where('api_key','=',$apiKey)->firstOrFail();

        $job->template = 'orderDelete';
        $job->content = urldecode(Input::get('content'));
        $job->apiclient = $client;
        $job->save();

      $print = new PrintInstance;
      $print->addPrint($job);
      $print->sendToPrint($job,$client,$meta,$print->id,$duplicate);
    }









}
