<?php
Route::post('/services/print/order/new', 'EgerStudio\hyndlaApi\Classes\hyndlaPrintOrderController@newOrder');
Route::post('/services/print/order/update', 'EgerStudio\hyndlaApi\Classes\hyndlaPrintOrderController@updateOrder');
Route::post('/services/print/order/delete', 'EgerStudio\hyndlaApi\Classes\hyndlaPrintOrderController@deleteOrder');
Route::get('/services/print/clean/all', 'EgerStudio\hyndlaApi\Classes\hyndlaPrintSupervisorController@simpleCleanAndPrintExt');
