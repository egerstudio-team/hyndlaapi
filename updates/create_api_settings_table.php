<?php namespace EgerStudio\HyndlaAPI\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateApiSettingsTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_hyndlaapi_api_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_hyndlaapi_api_settings');
    }

}
