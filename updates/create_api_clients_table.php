<?php namespace EgerStudio\HyndlaAPI\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateApiClientsTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_hyndlaapi_api_clients', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('balder_id');
            $table->string('api_key');
            $table->string('vpn_host');
            $table->string('trym_host');
            $table->string('meta_location');
            $table->text('extended');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_hyndlaapi_api_clients');
    }

}
