<?php namespace EgerStudio\HyndlaApi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePrintJobsTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_hyndlaapi_print_jobs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('template');
            $table->text('content');
            $table->integer('api_client_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_hyndlaapi_print_jobs');
    }

}
