<?php namespace EgerStudio\HyndlaApi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateJanitorsTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_hyndlaapi_janitors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_hyndlaapi_janitors');
    }

}
