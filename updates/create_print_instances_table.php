<?php namespace EgerStudio\HyndlaApi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePrintInstancesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_hyndlaapi_print_instances', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->boolean('printed');
            $table->timestamp('printed_at');
            $table->integer('print_job_id')->unsigned()->index();
            $table->text('response');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_hyndlaapi_print_instances');
    }

}
