<?php namespace EgerStudio\HyndlaAPI;

use System\Classes\PluginBase;
use Backend;
use EgerStudio\HyndlaApi\Models\Janitor;

/**
 * HyndlaAPI Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'HyndlaAPI',
            'description' => 'No description provided yet...',
            'author'      => 'EgerStudio',
            'icon'        => 'icon-leaf'
        ];
    }


    public function registerNavigation()
    {
        return [
            'hyndlaapi' => [
                'label'       => 'Hyndla API',
                'url'         => Backend::url('egerstudio/hyndlaapi/apiclients'),
                'icon'        => 'icon-archive',
                'permissions' => ['egerstudio.hyndlaapi.*'],
                'order'       => 300,

                'sideMenu' => [
                    'apiclients' => [
                        'label'       => 'API Clients',
                        'icon'        => 'icon-users',
                        'url'         => Backend::url('egerstudio/hyndlaapi/apiclients'),
                        'permissions' => ['egerstudio.hyndlaapi.apiclients']
                    ],
                    'printjobs' => [
                        'label'       => 'Print jobs',
                        'icon'        => 'icon-print',
                        'url'         => Backend::url('egerstudio/hyndlaapi/printjobs'),
                        'permissions' => ['egerstudio.hyndlaapi.printjobs']
                    ],
                  ]
              ]
          ];
      }

      public function registerPermissions()
      {
          return [
              'egerstudio.hyndlaapi.settings' => [
                  'label' => 'Manage HyndlaAPI global settings',
                  'tab' => 'HyndlaAPI'
              ],
              'egerstudio.hyndlaapi.apiclients' => [
                  'label' => 'Manage HyndlaAPI clients',
                  'tab' => 'HyndlaAPI'
              ],
              'egerstudio.hyndlaapi.printjobs' => [
                  'label' => 'Manage HyndlaAPI Print jobs',
                  'tab' => 'HyndlaAPI',
              ],
          ];
      }


      public function registerSettings(){
      return [
          'settings' => [
              'label'       => 'Hyndla API',
              'description' => 'Manage Hyndla API settings',
              'icon'        => 'icon-bar-chart-o',
              'class'       => 'EgerStudio\HyndlaAPI\Models\ApiSettings',
              'order'       => 1
            ]
        ];
    }


    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
          $janitor = new Janitor;
          $janitor->simpleCleanAndPrint();
        })->cron('* * * * *');
    }






}
