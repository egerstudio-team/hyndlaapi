<?php namespace EgerStudio\HyndlaApi\Models;

use Model;
use EgerStudio\HyndlaAPI\Models\PrintJob;
use EgerStudio\HyndlaAPI\Models\ApiClient;
use EgerStudio\HyndlaAPI\Models\ApiSettings;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Log as SystemLog;

/**
 * PrintInstance Model
 */
class PrintInstance extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_hyndlaapi_print_instances';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['id','printed','response','printed_at'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['printjob' => ['EgerStudio\HyndlaAPI\Models\PrintJob', 'key' => 'print_job_id']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function addPrint(PrintJob $job){

      $this->print_job_id = $job->id;
      $this->save();
      // now see if we should print

    }


    public function sendToPrint(PrintJob $job,ApiClient $client,$meta,$id,$duplicate){

      $contentVisible = json_decode(urldecode($job->content),true);
      $content = $job->content;
      $dest = $client->vpn_host;
      $template = $job->template;

      $printSend = $this->printThisInstance($dest,$template,$content,$meta,$job->id,$duplicate);
      $this->response = $printSend;
      $this->save();

    }


    public function printThisInstance($dest,$template,$content,$meta,$pj,$duplicate) {


      $requestBinClient = new Client;
      $printClient = new Client;
      $testClient = new Client;
      $settings = ApiSettings::instance();


      try{
        if($settings['useRequestBin'] == 1) {
          $response2 = $requestBinClient->post($settings['requestBinUrl'],[
            'query' => [
              'print' => $template
            ],
            'body' => [
              'content' => $content,
              'meta' => $meta,
              'pj' => $pj,
              'pi' => $this->id,
              'duplicate' => $duplicate
            ],
            'exceptions' => FALSE
            ]);
            $statuscode = $response2->getStatusCode();

          }

        $response = $printClient->post('http://'.$dest.'/app/services/print/',[
          'query' => [
            'print' => $template
          ],
          'body' => [
            'content' => $content,
            'meta' => $meta,
            'pj' => $pj,
            'pi' => $this->id,
            'duplicate' => $duplicate
          ],
          'exceptions' => FALSE
          ]);
          SystemLog::info('Printclient sent ('.$pj.'-'.$this->id.') and got return status code '.$response->getStatusCode());

          $statuscode = $response->getStatusCode();
          if($statuscode == 200) {
            $this->printed = '1';
            $this->response =  $response->getStatusCode();
            $this->printed_at = Carbon::now();
            $this->save();
            return $response;
          } else {
            return false;
          }


        } catch (RequestException $e) {
          SystemLog::info('Error occured: '.$e->getResponse()->getBody());
          return false;
        }




        //we sent a request, let us log what we got back

    SystemLog::info('We are returning $printOrder back to Trym ('.$response->getStatusCode().')');




    }

}
