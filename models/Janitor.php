<?php namespace EgerStudio\HyndlaApi\Models;

use Model;
use EgerStudio\HyndlaApi\Models\ApiClient;
use EgerStudio\HyndlaApi\Models\PrintJob;
use EgerStudio\HyndlaApi\Models\PrintInstance;
use EgerStudio\HyndlaApi\Models\ApiSettings;
use GuzzleHttp\Client;
use Log as SystemLog;

/**
 * Janitor Model
 */
class Janitor extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_hyndlaapi_janitors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];



    public function simpleCleanAndPrint(){

      SystemLog::info('Running simple clean and print');
      /* run this function do clean up all dead print jobs and reprint them
      */

      $clients = ApiClient::with(['printjobs.printinstances' => function($query) {
          $query->where('printed','=',0);
        }
      ])->get();


      foreach($clients as $client) {
          SystemLog::info('Cleaning and printing for client '.$client->id);


          // get meta
          $meta = $this->getMeta($client);
          $meta['fromQueue'] = 1;
          $meta = http_build_query($meta);


          foreach($client->printjobs as $job) {


            foreach($job->printinstances as $instance) {
                $contentVisible = json_decode(urldecode($job->content),true);
                $content = $job->content;
                $dest = $client->vpn_host;
                $template = $job->template;

                $printSend = $instance->printThisInstance($dest,$template,$content,$meta,$job->id,0);
                SystemLog::info('Printing from queue: '.$job->id.'-'.$instance->id);
                $instance->response = $printSend;
                $instance->save();
              }
          }

      }
      SystemLog::info('Simple clean and print done');

    }


    private function getMeta(ApiClient $client) {

      $metaClient = new Client;

      if($client->trym_host) {
        $response = $metaClient->get('http://'.$client->trym_host.$client->meta_location,[
          'query' => [
            'balderId' => $client->balder_id,
          ],
          'exceptions' => FALSE
        ]);
        $statuscode = $response->getStatusCode();
        SystemLog::info('Ran getMeta with '.$client->balder_id.' and got '.$statuscode.' in return ('.$client->trym_host.$client->meta_location.')');
        /*
        if (200 == $statuscode) {
          print('200');
        }
        elseif (304 == $statuscode) {
          print('304');
        }
        elseif (404 == $statuscode) {
          print('404');
        }
        elseif (500 == $statuscode) {
          print('500');
        } else {
          print('other');
        }*/
        return json_decode($response->getBody()->getContents(),true);
      } else {
        SystemLog::info('No meta running, trym host missing in config for client '.$client->name);
      }
        
    }

}
