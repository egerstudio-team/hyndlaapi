<?php namespace EgerStudio\HyndlaAPI\Models;

use Model;

/**
 * ApiClient Model
 */
class ApiClient extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_hyndlaapi_api_clients';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['printjobs' => ['EgerStudio\HyndlaAPI\Models\PrintJob','key' => 'api_client_id']];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
