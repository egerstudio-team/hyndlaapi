<?php namespace EgerStudio\HyndlaApi\Models;

use Model;

/**
 * PrintJob Model
 */
class PrintJob extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_hyndlaapi_print_jobs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['printinstances' => ['EgerStudio\HyndlaAPI\Models\PrintInstance']];
    public $belongsTo = ['apiclient' => ['EgerStudio\HyndlaAPI\Models\ApiClient', 'key' => 'api_client_id']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
