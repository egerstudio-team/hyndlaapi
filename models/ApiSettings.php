<?php namespace EgerStudio\HyndlaAPI\Models;

use Model;

/**
 * ApiSettings Model
 */
class ApiSettings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'egerstudio_hyndlaapi_settings';
    public $settingsFields = 'fields.yaml';

}
